<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nquantityo:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css" rel="stylesheet">
    <link rel="icon" href="/svg/adjust.svg" type="image/svg">
    <link rel="stylesheet" type="text/css" href="/css/lightpick.css">
</head>

<body class="flex">
    <section class="hidden lg:block w-64 bg-gray-500">
        <div class="px-10 py-24 fixed h-full z-10">
            <p class="text-base text-black font-bold px-4 mb-1">Expense</p>
            <div class="flex flex-col text-black text-sm">
                <a href="{{ route('expenses.create') }}" class="no-underline mt-2 cursor-pointer {{ ($page == 'expense-create')? 'text-white' : 'text-black' }}  font-bold px-4 hover:font-extrabold ml-8">
                    Create
                </a>
                <a href="#" class="no-underline mt-2 cursor-pointer text-black font-bold px-4 hover:font-extrabold ml-8">
                    List
                </a>
            </div>
        </div>
    </section>
    <section class="w-full body-font">
        <div class="container-fluid px-5 pt-5 pb-5 border-b border-gray-200">
            <nav class="">
                <div class="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
                    <div class="relative flex items-center justify-between h-auto">
                        <div class="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
                        </div>
                        <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
                            <button class="p-1 border-2 border-transparent text-gray-400 rounded-full hover:text-white focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out" aria-label="Notifications">
                                <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                                </svg>
                            </button>

                            <!-- Profile dropdown -->
                            <div class="ml-3 relative">
                                <div>
                                    <button class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-white transition duration-150 ease-in-out" id="user-menu" aria-label="User menu" aria-haspopup="true">
                                        <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                                    </button>
                                </div>
                                <div class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg">
                                    <div class="py-1 rounded-md bg-white shadow-xs" role="menu" aria-orientation="vertical" aria-labelledby="user-menu">
                                        <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">Your Profile</a>
                                        <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">Settings</a>
                                        <a href="#" class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out">Sign out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div class="container-fluid px-5 py-3 border-b border-gray-200">
            <div class="flex flex-col sm:flex-row sm:items-center items-start">
                <h1 class="flex-grow title-font text-gray-900"><a class="btn hover:text-blue-300" href="{{ route('home') }}">Dashboard</a> | <a class="btn hover:text-blue-300" href="#">Expenses</a> | Create</h1>
            </div>
        </div>
        @if(session('success') or session('failed'))
        <div id="alert">
            <div class="container-fluid px-5 pt-5 text-center bg-gray-200">
                <div class="lg:w-1/2 mx-auto {{ session('success')? 'bg-green-100 border border-green-400 text-green-700!' : 'bg-red-100 border border-red-400 text-red-700' }} px-4 py-3 rounded relative" role="alert">
                    <strong class="font-bold">{{ session('success')? 'Success!' : 'Failed!' }}</strong>
                    <span class="block sm:inline">{{ session('success')? session('success') : session('failed') }}</span>
                    <span class="absolute top-0 bottom-0 right-0 px-4 py-3" onclick="alertDisappear()">
                        <svg class="fill-current h-6 w-6 text-red-500" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <title>Close</title>
                            <path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" />
                        </svg>
                    </span>
                </div>
            </div>
        </div>
        @endif
        <div class="container-fluid px-5 pt-5 pb-24 mx-auto bg-gray-200">
            <div class="flex flex-wrap -mx-4 -mb-10 text-center">
                <div class="w-full mx-auto mb-10 px-4">
                    <form action="{{ route('expenses.store') }}" method="POST" class="lg:w-1/2 md:w-full px-10 pt-10 pb-5 bg-white rounded-lg shadow-xl border" id="expense_form_store">
                        @csrf
                        <span class="text-left block uppercase text-gray-700 text-2xl font-bold">Expense Info:</span>
                        <div class="md:flex md:items-center py-5">
                            <div class="md:w-1/4 relative">
                                <img class="absolute h-6 right-0 top-0 cursor-pointer" onclick="modalOn('member')" src="/svg/add-outline.svg" alt="" title="Add New Member">
                                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="member">
                                    Name
                                </label>
                            </div>
                            <div class="md:w-1/4">
                                <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="member" value="{{ old('member') }}" id="member" placeholder="Enter Member" list="members" autocomplete="off" required>
                                <datalist id="members">
                                    @foreach($members as $member)
                                    <option value="{{ $member->name }}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                            <div class="md:w-1/4">
                                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="datepicker">
                                    Date
                                </label>
                            </div>
                            <div class="md:w-1/4">
                                <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="date" id="datepicker" value="{{ date('d/m/Y') }}">
                            </div>
                        </div>
                        <div class="md:flex md:items-center pb-5">
                            <div class="md:w-1/4">
                                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="category">
                                    Category
                                </label>
                            </div>
                            <div class="md:w-1/4">
                                <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="category" id="category" placeholder="Select Category" list="categories" autocomplete="off" onchange="categoryChange()">
                                <datalist id="categories">
                                    @foreach($categories as $category)
                                    <option value="{{ $category->name }}">{{ $category->name }}</option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div id="general_paid">
                            <span class="text-left block uppercase text-gray-700 font-bold">Payment Info:</span>
                            <div class="md:flex md:items-center pb-5">
                                <div class="md:w-1/4">
                                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="paid">
                                        Paid
                                    </label>
                                </div>
                                <div class="md:w-1/4">
                                    <div class="relative rounded-md shadow-sm">
                                        <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="paid" id="paid" placeholder="Enter Paid" onkeyup="paymentCalculate()" required>
                                        <div class="absolute inset-y-0 right-0 flex items-center">
                                            <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent">
                                                <option value="tk">Tk</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="for_purchase">
                        </div>
                        <div class="flex flex-wrap -mx-3">
                            <div class="mx-3 ml-auto inline-flex rounded-md shadow">
                                <button type="submit" class="bg-blue-500 hover:bg-blue-700 block uppercase tracking-wide text-white text-xs font-bold rounded px-8 py-2 focus:outline-none focus:shadow-outline">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                    <div id="modal_view">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script src="/js/lightpick.js"></script>
    <script src="/js/ajax.js"></script>
</body>

</html>
<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    return view('welcome');
});

Route::get('/', 'ExpenseController@create')->name('home');

Route::get('/expenses/create', 'ExpenseController@create')->name('expenses.create');
Route::post('/expenses/store', 'ExpenseController@store')->name('expenses.store');

Route::post('/members/store', 'MemberController@store')->name('members.store');

Route::get('/categories/{category}', 'CategoryController@details')->name('categories.details');

Route::get('/products/{product}', 'ProductController@details')->name('products.details');
Route::post('/products/store', 'ProductController@store')->name('products.store');

Auth::routes();

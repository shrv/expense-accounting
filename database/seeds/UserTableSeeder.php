<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'shooorov@gmail.com',
            'password' => bcrypt('shooorov@gmail.com'),
            'name'  => 'Shourov Shahadat',
            'created_at' => now(),
        ]);
    }
}

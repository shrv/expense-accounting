<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {

    $name           = $faker->unique()->randomElement($array = array('Apples', 'Bananas', 'Cherries', 'Dragon Fruit', 'Grapes', 'Jackfruit', 'Kiwi', 'Mango', 'Orange', 'Papaya'));
    $unit           = $faker->randomElement($array = array('Kg', 'Ltr', 'Pieces'));
    $slug           = Str::slug($name);
    return [
        'slug'          => $slug,
        'name'          => $name,
        'unit'          => $unit,
        'category_id'   => 1
    ];
});

{
    ("use strict");

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    let counting = 0;

    function alertDisappear() {
        document.getElementById("alert").innerHTML = ``;
    }

    function categoryChange() {
        let category = document.getElementById("category").value;
        if (category == "Purchase") {
            $.ajax({
                type: "get",
                url: "/categories/" + category
            })
                .done(function(response) {
                    let items = response.products;
                    if (items) {
                        document.getElementById("general_paid").innerHTML = ``;
                        document.getElementById("for_purchase").innerHTML =
                            `<span class="text-left block uppercase text-gray-700 text font-bold pb-4">Product List:</span>` +
                            productRow() +
                            `<div id="extra">
                            </div>
                            <span class="text-left block uppercase text-gray-700 text font-bold py-4">Payment Info:</span>
                            <div class="md:flex md:items-center pb-5">` +
                            commonField("sub_total") +
                            `<div class="md:w-1/4">
                                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="discount">
                                        Discount
                                    </label>
                                </div>
                                <div class="md:w-1/4">
                                    <div class="relative rounded-md shadow-sm">
                                        <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="discount" id="discount" placeholder="Enter" onkeyup="paymentCalculate()">
                                        <div class="absolute inset-y-0 right-0 flex items-center">
                                            <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent" name="discount_type" id="discount_type" onchange="paymentCalculate()">
                                                <option value="tk">Tk</option>
                                                <option value="percent">Percent</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="md:flex md:items-center pb-5">` +
                            commonField("grand_total") +
                            `</div>
                            <div class="md:flex md:items-center pb-5">
                                <div class="md:w-1/4">
                                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="paid">
                                        Paid
                                    </label>
                                </div>
                                <div class="md:w-1/4">
                                    <div class="relative rounded-md shadow-sm">
                                        <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="paid" id="paid" placeholder="Enter Paid" onkeyup="paymentCalculate()" required>
                                        <div class="absolute inset-y-0 right-0 flex items-center">
                                            <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent">
                                                <option value="tk">Tk</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>` +
                            commonField("due") +
                            `</div>`;

                        let options = ``;
                        for (let i = 0; i < Object.keys(items).length; i++) {
                            options +=
                                `<option value="` +
                                items[i].name +
                                `"></option>`;
                        }
                        document.getElementById(
                            "products_list"
                        ).innerHTML = options;
                    }
                })
                .fail(function(response) {
                    console.log("failed");
                });
        } else {
            document.getElementById(
                "general_paid"
            ).innerHTML = `<span class="text-left block uppercase text-gray-700 font-bold">Payment Info:</span>
                            <div class="md:flex md:items-center pb-5">
                                <div class="md:w-1/4">
                                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="paid">
                                        Paid
                                    </label>
                                </div>
                                <div class="md:w-1/4">
                                    <div class="relative rounded-md shadow-sm">
                                        <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="paid" id="paid" placeholder="Enter Paid" onkeyup="paymentCalculate()" required>
                                        <div class="absolute inset-y-0 right-0 flex items-center">
                                            <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent">
                                                <option value="tk">Tk</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
            document.getElementById("for_purchase").innerHTML = "";
        }
    }
    function commonField(fieldName) {
        let field =
            `<div class="md:w-1/4">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="` +
            fieldName +
            `">` +
            (fieldName == "sub_total"
                ? "Sub-total"
                : fieldName == "Grand-Total"
                ? "Grand-Total"
                : "Due") +
            `</label>
            </div>
            <div class="md:w-1/4">
                <div class="relative rounded-md shadow-sm">
                    <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="` +
            fieldName +
            `" id="` +
            fieldName +
            `" placeholder="` +
            (fieldName == "sub_total"
                ? "Sub-total"
                : fieldName == "Grand-Total"
                ? "Grand-Total"
                : "Due") +
            `" readonly>
                    <div class="absolute inset-y-0 right-0 flex items-center">
                        <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent">
                            <option value="tk">Tk</option>
                        </select>
                    </div>
                </div>
            </div>`;
        return field;
    }

    function paymentCalculate() {
        let category = document.getElementById("category").value;
        if (category == "Purchase") {
            let form = document.forms.expense_form_store;
            let prices = form.elements["prices[]"];

            let discount_type = document.getElementById("discount_type");
            discount_type =
                discount_type.options[discount_type.selectedIndex].value;
            let discount = Number(document.getElementById("discount").value);
            let paid = Number(document.getElementById("paid").value);

            if (prices.length == undefined) {
                prices = [prices];
            }

            let total = 0;
            prices.forEach(function(price, i) {
                if (prices[i].value != null) {
                    total += Number(prices[i].value);
                }
            });

            discount =
                discount_type == "tk" ? discount : (total * discount) / 100;
            let payable = total - discount;
            document.getElementById("sub_total").value = total;
            document.getElementById("grand_total").value = payable;
            document.getElementById("due").value = payable - paid;
        }
    }

    function modalOn(form) {
        let modal =
            `<div class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center">
                        <div class="fixed inset-0 transition-opacity">
                            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
                        </div>
                        <div class="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all lg:w-2/6">
                            <div class="float-right p-2">
                                <img src="/svg/close.svg" class="h-4 lg:mt-2 lg:mr-2 cursor-pointer" alt="" onclick="modalOff()">
                            </div>
                            <div class="bg-white px-4 pb-4">
                                <form class="w-full lg:px-16 py-6">
                                    <label class="block uppercase tracking-wide text-gray-700 lg:text-2xl font-bold mb-10">` +
            form +
            ` Create</label>` +
            (form == "member" ? formCreate(form) : formCreate(form)) +
            `
                                    <div class="flex flex-wrap -mx-3">
                                        <div class="mx-3 ml-auto inline-flex rounded-md shadow">
                                            <a onclick="` +
            form +
            `Store()" class="cursor-pointer bg-blue-500 hover:bg-blue-700 block uppercase tracking-wide text-white text-xs font-bold rounded px-6 py-2 focus:outline-none focus:shadow-outline">
                                                Create
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>`;
        document.getElementById("modal_view").innerHTML = modal;
    }

    function modalOff() {
        document.getElementById("modal_view").innerHTML = ``;
    }

    function formCreate(form) {
        let form_making;
        if (form == "member") {
            form_making =
                commonFieldModalForm(form, "member_name") +
                commonFieldModalForm(form, "member_mobile");
        } else {
            form_making =
                commonFieldModalForm(form, "product_name") +
                commonFieldModalForm(form, "product_unit") +
                commonFieldModalForm(form, "product_purchase");
        }
        return form_making;
    }
    function commonFieldModalForm(form, fieldName) {
        let field =
            `<div class="md:flex md:items-center pb-5">
                <div class="md:w-1/3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mr-5 mb-2" for="` +
            fieldName +
            `">` +
            (form == "member"
                ? fieldName == "member_name"
                    ? "Name"
                    : "Mobile"
                : fieldName == "product_name"
                ? "Name"
                : fieldName == "product_unit"
                ? "Unit"
                : "Category") +
            `</label>
                </div>
                <div class="md:w-2/3">
                    <input type="text" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="` +
            fieldName +
            `" id="` +
            fieldName +
            `" placeholder="Enter ` +
            (form == "member"
                ? fieldName == "member_name"
                    ? "Member Name"
                    : "Member Mobile"
                : fieldName == "product_name"
                ? "Product Name"
                : fieldName == "product_unit"
                ? "Product Unit"
                : "Product Purchase") +
            `" value="` +
            (fieldName == "product_purchase" ? "Purchase" : "") +
            `" ` +
            (fieldName == "product_purchase" ? "readOnly" : "") +
            ` autocomplete="off">
                </div>
            </div>`;
        return field;
    }

    function memberStore() {
        let member_name = document.getElementById("member_name").value;
        let member_mobile = document.getElementById("member_mobile").value;

        $.ajax({
            type: "post",
            url: "/members/store",
            data: {
                name: member_name,
                mobile: member_mobile
            }
        })
            .done(function(response) {
                let items = response;
                let options = ``;
                for (let i = 0; i < Object.keys(items).length; i++) {
                    options +=
                        `<option value="` + items[i].name + `"></option>`;
                }
                document.getElementById("members").innerHTML = options;
                modalOff();
            })
            .fail(function(response) {
                console.log("failed");
            });
    }

    function productStore() {
        let product_name = document.getElementById("product_name").value;
        let product_unit = document.getElementById("product_unit").value;

        $.ajax({
            type: "post",
            url: "/products/store",
            data: {
                name: product_name,
                unit: product_unit
            }
        })
            .done(function(response) {
                let items = response;
                let options = ``;
                for (let i = 0; i < Object.keys(items).length; i++) {
                    options +=
                        `<option value="` + items[i].name + `"></option>`;
                }
                document.getElementById("products_list").innerHTML = options;
                modalOff();
            })
            .fail(function(response) {
                console.log("failed");
            });
    }

    function productRow() {
        let product_row =
            `<div class="flex flex-wrap -mx-3 pb-5 relative">
                <img class="absolute h-6 right-0 top-0 cursor-pointer" onclick="productChildAddition()" src="/svg/add-outline.svg" alt="" title="Add Product">
                <div class="w-full md:w-1/3 px-3 mb-2 md:mb-0">
                    <img class="absolute h-6 left-0 top-0 cursor-pointer" onclick="modalOn('product')" src="/svg/add-outline.svg" alt="" title="Add New Product">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Product
                    </label>
                    <div class="relative">
                        <input type="text" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 p-2 pl-4 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="products[]" id="product" placeholder="Enter Product" list="products_list" autocomplete="off" required onchange="productChange(` +
            0 +
            `)">
                        <datalist id="products_list">
                        </datalist>
                    </div>
                </div>
                <div class="w-full md:w-1/3 px-3 mb-2 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Quantity
                    </label>
                    <div class="relative rounded-md shadow-sm">
                        <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="quantities[]" id="quantity" type="text" placeholder="Enter Quantity" required>
                        <div class="absolute inset-y-0 right-0 flex items-center">
                            <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent" id="unit">
                                <option>Unit</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="w-full md:w-1/3 px-3 mb-2 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                        Price
                    </label>
                    <div class="relative rounded-md shadow-sm">
                        <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="prices[]" id="price" type="text" placeholder="Enter Price" onkeyup="paymentCalculate()" required>
                        <div class="absolute inset-y-0 right-0 flex items-center">
                            <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent">
                                <option value="tk">Tk</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>`;
        return product_row;
    }

    function productChildAddition() {
        $.ajax({
            type: "get",
            url: "/categories/Purchase"
        })
            .done(function(response) {
                counting++;
                let data = "";
                let items = response.products;
                let options = ``;
                for (let i = 0; i < Object.keys(items).length; i++) {
                    options +=
                        `<option value="` + items[i].name + `"></option>`;
                }
                data +=
                    `<div class="flex flex-wrap -mx-3 pb-5 relative">
                        <img class="absolute h-6 right-0 bottom-0 cursor-pointer" onclick="productChildDelete(` +
                    counting +
                    `)" src="/svg/edit-cut.svg" alt="" title="Remove Product">` +
                    productChildProduct(counting) +
                    productChildQuantity(counting) +
                    productChildPrice(counting) +
                    `</div>`;
                var div = document.createElement("div");
                div.id = "content_" + counting;
                div.innerHTML = data;
                document.getElementById("extra").appendChild(div);

                productDataSwap(counting);
            })
            .fail(function(response) {
                console.log("failed");
            });
    }
    function productChildProduct(id) {
        let product =
            `<div class="w-full md:w-1/3 px-3 mb-2 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Product
                </label>
                <div class="relative">
                    <input type="text" class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 p-2 pl-4 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="products[]" id="product_` +
            id +
            `" placeholder="Enter Product" list="products_list" autocomplete="off" required onchange="productChange(` +
            id +
            `)">
                    <datalist id="products_list">
                    </datalist>
                </div>
            </div>`;
        return product;
    }
    function productChildQuantity(id) {
        let quantity =
            `<div class="w-full md:w-1/3 px-3 mb-2 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Quantity
                </label>
                <div class="relative rounded-md shadow-sm">
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="quantities[]" id="quantity_` +
            id +
            `" type="text" placeholder="Enter Quantity" required>
                    <div class="absolute inset-y-0 right-0 flex items-center">
                        <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent" id="unit_` +
            id +
            `">
                            <option>Unit</option>
                        </select>
                    </div>
                </div>
            </div>`;

        return quantity;
    }
    function productChildPrice(id) {
        let price =
            `<div class="w-full md:w-1/3 px-3 mb-2 md:mb-0">
                <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                    Price
                </label>
                <div class="relative rounded-md shadow-sm">
                    <input class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" name="prices[]" id="price_` +
            id +
            `" type="text" placeholder="Enter Price" onkeyup="paymentCalculate()" required>
                    <div class="absolute inset-y-0 right-0 flex items-center">
                        <select class="w-full appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-2 px-4 leading-tight focus:outline-none focus:bg-white border-gray-500 bg-transparent">
                            <option value="tk">Tk</option>
                        </select>
                    </div>
                </div>
            </div>`;

        return price;
    }
    function productDataSwap(id) {
        let product = document.getElementById("product");
        let quantity = document.getElementById("quantity");
        let unit = document.getElementById("unit");
        let unit_value = unit.options[unit.selectedIndex].value;
        let price = document.getElementById("price");
        document.getElementById("product_" + id).value = product.value;
        document.getElementById("quantity_" + id).value = quantity.value;
        document.getElementById("unit_" + id).innerHTML =
            `<option>` + unit_value + `</option>`;
        document.getElementById("price_" + id).value = price.value;
        product.value = ``;
        quantity.value = ``;
        unit.innerHTML = `<option>Unit</option>`;
        price.value = ``;
    }

    function productChildDelete(id) {
        var element = document.getElementById("content_" + id);
        element.parentNode.removeChild(element);
        paymentCalculate();
    }

    function productChange(id) {
        let product = "";
        if (id == 0) {
            product = document.getElementById("product").value;
        } else {
            product = document.getElementById("product_" + id).value;
        }
        $.ajax({
            type: "get",
            url: "/products/" + product
        })
            .done(function(response) {
                console.log(response);
                let unit = response.unit ? response.unit : "not set yet";
                if (id == 0) {
                    document.getElementById("unit").innerHTML =
                        `<option>` + unit + `</option>`;
                } else {
                    document.getElementById("unit_" + id).innerHTML =
                        `<option>` + unit + `</option>`;
                }
            })
            .fail(function(response) {
                console.log("failed");
            });
    }

    var picker = new Lightpick({
        field: document.getElementById("datepicker"),
        onSelect: function(date) {
            document.getElementById("datepicker").innerHTML = date.format(
                "Do MMMM YYYY"
            );
        }
    });
}

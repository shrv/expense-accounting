<?php

namespace App\Http\Controllers;

use App\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    public function store(Request $request)
    {
        $member = new Member;
        $member->name = $request->name;
        $member->mobile = $request->mobile;
        $member->slug = Str::slug($request->name);
        if ($member->save()) {
            return Member::all();
        }
        return json_encode(false);
    }
}

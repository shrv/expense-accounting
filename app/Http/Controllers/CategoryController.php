<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function details(Request $request)
    {
        $category = Category::where('name', $request->category)->first();
        // $category->products();
        $category->products = Product::where('category_id', $category->id)->get();
        return $category;
    }
}

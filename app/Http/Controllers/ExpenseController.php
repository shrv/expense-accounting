<?php

namespace App\Http\Controllers;

use App\Category;
use App\Expense;
use App\ExpenseItem;
use App\Member;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use function GuzzleHttp\json_decode;

class ExpenseController extends Controller
{
    public function create()
    {
        $param = [
            'products'      => Product::all(),
            'members'       => Member::all(),
            'categories'    => Category::all(),
            'page'          => 'expense-create'
        ];
        return view('dashboard.expense-create', $param);
    }

    public function store(Request $request)
    {
        $expense = new Expense;

        if (!empty($request->member)) {
            $member = Member::where('name', $request->member)->first();
            if ($member) {
                $expense->member_id   = $member->id;
            } else {
                $member = new Member;
                $member->name = $request->member;
                $member->slug = Str::slug($request->name);
                $member->save();
            }
        }

        $purchase_type = false;
        if (!empty($request->category)) {
            $category = Category::where('name', $request->category)->first();
            if ($category) {
                $expense->category_id   = $category->id;
            } else {
                $category = new Category;
                $category->name = $request->category;
                $category->slug = Str::slug($request->category);
                $category->save();
            }
            $purchase_type = $category->name == 'Purchase' ? true : false;
        }

        if ($purchase_type) {
            $expense->grand_total   = isset($request->grand_total) ? $request->grand_total : null;
            $expense->sub_total     = isset($request->sub_total) ? $request->sub_total : null;
            $expense->due           = isset($request->due) ? $request->due : null;
        }
        $expense->paid  = isset($request->paid) ? $request->paid : null;

        if (isset($request->date)) {
            $date_array = explode('/', $request->date);
            $expense->date = $date_array[2] . "-" . $date_array[1] . "-" . $date_array[0];
        } else {
            $expense->date = date('Y-m-d');
        }

        $expense->discount      = isset($request->discount) ? $request->discount : null;
        $expense->discount_type = isset($request->discount_type) ? $request->discount_type : null;
        if ($expense->save()) {

            if ($purchase_type) {

                for ($i = 0; $i < count($request->products); $i++) {
                    if ($request->products[$i] != null and $request->prices[$i] != null and $request->quantities[$i] != null) {
                        $expense_item = new ExpenseItem;
                        $expense_item->expense_id   = $expense->id;

                        $product = Product::where('name', $request->products[$i])->first();
                        if (!$product) {
                            $product    = new Product;
                            $product->name  = $request->products[$i];
                            $product->unit  = 'not set yet';
                            $product->slug  = Str::slug($request->products[$i]);
                            $product->category_id = 1; // default - right now #1 is Purchase
                            $product->save();
                        }
                        $expense_item->product_id   = $product->id;
                        $expense_item->quantity     = $request->quantities[$i];
                        $expense_item->price        = $request->prices[$i];
                        $expense_item->save();
                    }
                }
                $expense->expense_items;
            }
            return redirect()->route('expenses.create')->with('success', 'Expense created! You may create another expense.');
        }
        return redirect()->route('expenses.create')->with('failed', 'Expense creating failed!.');
    }
}

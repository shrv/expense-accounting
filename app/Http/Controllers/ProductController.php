<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function list()
    {
        return Product::all();
    }

    public function details(Request $request)
    {
        $product = Product::where('name', $request->product)->first();
        return $product ? $product : json_encode(false);
    }

    public function store(Request $request)
    {
        $product = new Product;
        $product->name = $request->name;
        $product->unit = $request->unit;
        $product->category_id = 1; // default - right now #1 is Purchase
        $product->slug = Str::slug($request->name);
        if ($product->save()) {
            return Product::all();
        }
        return json_encode(false);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Category;
use App\Expense;
use App\ExpenseItem;
use App\Member;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use function GuzzleHttp\json_decode;

class HomeController extends Controller
{
    public function create()
    {
        $categories = Category::all();
        $member = Member::all();
        $param = [
            'products'      => Product::all(),
            'members'       => $member,
            'categories'    => $categories,
        ];
        return $param;
    }

    public function store(Request $request)
    {
        $expense = new Expense;

        if (!empty($request->member)) {
            $member = Member::where('name', $request->member)->first();
            if ($member) {
                $expense->member_id   = $member->id;
            } else {
                $member = new Member;
                $member->name = $request->member;
                $member->slug = Str::slug($request->member);
                $member->save();
            }
        }

        $purchase_type = false;
        if (!empty($request->category)) {
            $category = Category::where('name', $request->category)->first();
            if ($category) {
                $expense->category_id   = $category->id;
            } else {
                $category = new Category;
                $category->name = $request->category;
                $category->slug = Str::slug($request->category);
                $category->save();
            }
            $purchase_type = $category->name == 'Purchase' ? true : false;
        }

        $expense->grand_total   = isset($request->grand_total) ? $request->grand_total : null;
        $expense->sub_total     = isset($request->sub_total) ? $request->sub_total : null;
        $expense->due           = isset($request->due) ? $request->due : null;
        $expense->paid  = isset($request->paid) ? $request->paid : null;

        if (isset($request->date)) {
            $date_array = explode('/', $request->date);
            $expense->date = $date_array[2] . "-" . $date_array[1] . "-" . $date_array[0];
        } else {
            $expense->date = date('Y-m-d');
        }

        $expense->note          = isset($request->note) ? $request->note : null;
        $expense->discount      = isset($request->discount) ? $request->discount : null;
        $expense->discount_type = isset($request->discount_type) ? $request->discount_type : null;
        if ($expense->save()) {

            if ($purchase_type) {
                $products = json_decode($request->products);
                $quantities = json_decode($request->quantities);
                $prices = json_decode($request->prices);
                for ($i = 0; $i < count($products); $i++) {
                    if ($products[$i] != null and $prices[$i] != null and $quantities[$i] != null) {
                        $expense_item = new ExpenseItem;
                        $expense_item->expense_id       = $expense->id;

                        $product = Product::where('name', $request->product[$i])->first();
                        if (!$product) {
                            $product = new Product;
                            $product->name = $products[$i];
                            $product->slug = Str::slug($products[$i]);
                            $product->unit = 'not set yet';
                            $product->category_id = 1;
                            $product->save();
                        }
                        $expense_item->product_id   = $product->id;

                        $expense_item->quantity     = $quantities[$i];
                        $expense_item->price        = $prices[$i];
                        $expense_item->save();
                    }
                }
                $expense->expense_items;
                foreach ($expense->expense_items as $item) {
                    $item->product_name = Product::find($item->product_id)->name;
                }
            }
            $info = [
                'expense'   => $expense,
                'status'    => 'successful'
            ];
            return $info;
        }
        return json_encode(false);
    }

    public function product_store(Request $request)
    {
        $product = new Product;
        $product->name = $request->name;
        $product->unit = isset($request->unit) ? $request->unit : null;
        $product->category_id = 1; // default - right now #1 is Purchase
        $product->slug = Str::slug($request->name);
        if ($product->save()) {
            return Product::all();
        }
        return json_encode(false);
    }

    public function member_store(Request $request)
    {
        $member = new Member;
        $member->name = $request->name;
        $member->mobile = $request->mobile;
        $member->slug = Str::slug($request->member);
        if ($member->save()) {
            return Member::all();
        }
        return json_encode(false);
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function api_login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $params = [
                'user'      => Auth::user(),
                'status'    => 'success',
                'message'   => 'Succefully Login.',
                'token'     => 'asdf345sfd4'
            ];
            return $params;
        }
    }

    public function scan(Request $request)
    {
        $params = [
            'user'      => User::find($request->id),
            'string'    => $request->string,
            'status'    => 'failed',
            'message'   => 'You\'ve Scaned an old QR Code.',
        ];
        return $params;
    }
}
